package extreme.fizzbuzz;

public class FizzBuzz5 {
    public static final String Fizz = "Fizz";
    public static final String Buzz = "Buzz";
    public static final String Whizz = "Whizz";
    public static final int Fizz_Number = 3;
    public static final int Buzz_Number = 5;
    public static final int Whizz_Number = 7;

    public String say(int number) {
        String result = "";
        if (isModByNumber(number, Fizz_Number)&&!String.valueOf(number).contains(String.valueOf(Buzz_Number))) {
            result += Fizz;
        }
        if (isModByNumber(number, Buzz_Number)) {
            result += Buzz;
        }
        if (isModByNumber(number, Whizz_Number)) {
            result += Whizz;
        }
        if (String.valueOf(number).contains(String.valueOf(Fizz_Number))) {
            result = Fizz;
        }
        return (result.isEmpty()) ?
                String.valueOf(number) : result;

    }

    private boolean isModByNumber(int target, int divisor) {
        return target % divisor == 0;
    }
}
