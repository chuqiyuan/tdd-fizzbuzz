package extreme.fizzbuzz;


public class FizzBuzz2 {
    public static final String Fizz = "Fizz";
    public static final String Buzz = "Buzz";
    public static final String Whizz = "Whizz";
    public static final int Fizz_Number = 3;
    public static final int Buzz_Number = 5;
    public static final int Whizz_Number = 7;

    public String say(int number) {
        if (isModByNumber(number, Fizz_Number)) {
            return Fizz;
        }
        if (isModByNumber(number, Buzz_Number)) {
            return Buzz;
        }
        if (isModByNumber(number, Whizz_Number)) {
            return Whizz;
        }
        return String.valueOf(number);
    }

    private boolean isModByNumber(int target, int divisor) {
        return target % divisor == 0;
    }
}
