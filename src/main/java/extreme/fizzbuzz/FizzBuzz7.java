package extreme.fizzbuzz;

public class FizzBuzz7 {
    public static final String Fizz = "Fizz";
    public static final String Buzz = "Buzz";
    public static final String Whizz = "Whizz";
    public static final int Fizz_Number = 3;
    public static final int Buzz_Number = 5;
    public static final int Whizz_Number = 7;

    public String say(int number) {
        String result = "";
        if (isModByNumber(number, Fizz_Number)&&(!iscontain(number,Buzz_Number)||iscontain(number,Whizz_Number))) {
            result += Fizz;
        }
        if (isModByNumber(number, Buzz_Number)&&!iscontain(number,Whizz_Number)) {
            result += Buzz;
        }
        if (isModByNumber(number, Whizz_Number)) {
            result += Whizz;
        }
        return (result.isEmpty()) ?
                String.valueOf(number) : result;

    }

    private boolean isModByNumber(int target, int divisor) {
        return target % divisor == 0;
    }
    private boolean iscontain(int number, int includeNumbers) { return String.valueOf(number).contains(String.valueOf(includeNumbers)); }

}
