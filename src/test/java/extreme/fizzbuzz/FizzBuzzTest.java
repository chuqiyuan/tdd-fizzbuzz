package extreme.fizzbuzz;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class FizzBuzzTest {

    @Test
    public void should_return_original_number_1_when_say_given_number_is_not_mod_by_3_5_7_and_not_contains_3_5_7(){
        FizzBuzz fizzBUzz = new FizzBuzz();
        String result = fizzBUzz.say(1);
        Assertions.assertEquals("1",result);
    }

    @Test
    public void should_return_original_number_4_when_say_given_number_is_not_mod_by_3_5_7_and_not_contains_3_5_7(){
        FizzBuzz fizzBUzz = new FizzBuzz();
        String result = fizzBUzz.say(4);
        Assertions.assertEquals("4",result);
    }

    @Test
    public void should_return_Fizz_when_say_given_number_is_only_mod_by_3_and_not_contains_3_5_7(){
        FizzBuzz2 fizzBUzz2 = new FizzBuzz2();
        String result = fizzBUzz2.say(6);
        Assertions.assertEquals("Fizz",result);
    }

    @Test
    public void should_return_Buzz_when_say_given_number_is_only_mod_by_5_and_not_contains_3_5_7(){
        FizzBuzz2 fizzBUzz2 = new FizzBuzz2();
        String result = fizzBUzz2.say(10);
        Assertions.assertEquals("Buzz",result);
    }

    @Test
    public void should_return_Whizz_when_say_given_number_is_only_mod_by_7_and_not_contains_3_5_7(){
        FizzBuzz2 fizzBUzz2 = new FizzBuzz2();
        String result = fizzBUzz2.say(14);
        Assertions.assertEquals("Whizz",result);
    }

    @Test
    public void should_return_FizzBuzz_when_say_given_number_is_mod_by_3_5_and_not_contains_3_5_7(){
        FizzBuzz3 fizzBUzz3 = new FizzBuzz3();
        String result = fizzBUzz3.say(60);
        Assertions.assertEquals("FizzBuzz",result);
    }

    @Test
    public void should_return_FizzWhizz_when_say_given_number_is_mod_by_3_7_and_not_contains_3_5_7(){
        FizzBuzz3 fizzBUzz3 = new FizzBuzz3();
        String result = fizzBUzz3.say(21);
        Assertions.assertEquals("FizzWhizz",result);
    }

    @Test
    public void should_return_BuzzWhizz_when_say_given_number_is_mod_by_5_7_and_not_contains_3_5_7(){
        FizzBuzz3 fizzBUzz3 = new FizzBuzz3();
        String result = fizzBUzz3.say(140);
        Assertions.assertEquals("BuzzWhizz",result);
    }

    @Test
    public void should_return_FizzBuzzWhizz_when_say_given_number_is_mod_by_3_5_7_and_not_contains_3_5_7(){
        FizzBuzz3 fizzBUzz3 = new FizzBuzz3();
        String result = fizzBUzz3.say(210);
        Assertions.assertEquals("FizzBuzzWhizz",result);
    }

    @Test
    public void should_return_Fizz_when_say_given_number_is_mod_by_5_and_only_contains_3(){
        FizzBuzz4 fizzBUzz4 = new FizzBuzz4();
        String result = fizzBUzz4.say(130);
        Assertions.assertEquals("Fizz",result);
    }

    @Test
    public void should_return_Fizz_when_say_given_number_is_mod_by_7_and_only_contains_3(){
        FizzBuzz4 fizzBUzz4 = new FizzBuzz4();
        String result = fizzBUzz4.say(133);
        Assertions.assertEquals("Fizz",result);
    }
    @Test
    public void should_return_Fizz_when_say_given_number_is_mod_by_3_5_and_only_contains_3(){
        FizzBuzz4 fizzBUzz4 = new FizzBuzz4();
        String result = fizzBUzz4.say(30);
        Assertions.assertEquals("Fizz",result);
    }
    @Test
    public void should_return_Fizz_when_say_given_number_is_mod_by_3_7_and_only_contains_3(){
        FizzBuzz4 fizzBUzz4 = new FizzBuzz4();
        String result = fizzBUzz4.say(63);
        Assertions.assertEquals("Fizz",result);
    }

    @Test
    public void should_return_Fizz_when_say_given_number_is_mod_by_3_5_7_and_only_contains_3(){
        FizzBuzz4 fizzBUzz4 = new FizzBuzz4();
        String result = fizzBUzz4.say(630);
        Assertions.assertEquals("Fizz",result);
    }

    @Test
    public void should_return_Fizz_when_say_given_number_is_not_mod_by_3_5_7_and_only_contains_3(){
        FizzBuzz4 fizzBUzz4 = new FizzBuzz4();
        String result = fizzBUzz4.say(1330);
        Assertions.assertEquals("Fizz",result);
    }

    @Test
    public void should_return_Fizz_when_say_given_number_is_mod_by_3_5_7_and_only_contains_3$2(){
        FizzBuzz4 fizzBUzz4 = new FizzBuzz4();
        String result = fizzBUzz4.say(13);
        Assertions.assertEquals("Fizz",result);
    }

    @Test
    public void should_return_51_when_say_given_number_is_mod_by_3_and_only_contains_5(){
        FizzBuzz5 fizzBUzz5 = new FizzBuzz5();
        String result = fizzBUzz5.say(51);
        Assertions.assertEquals("51",result);
    }

    @Test
    public void should_return_Buzz_when_say_given_number_is_mod_by_5_and_only_contains_5(){
        FizzBuzz5 fizzBUzz5 = new FizzBuzz5();
        String result = fizzBUzz5.say(5);
        Assertions.assertEquals("Buzz",result);
    }

    @Test
    public void should_return_Whizz_when_say_given_number_is_mod_by_7_and_only_contains_5(){
        FizzBuzz5 fizzBUzz5 = new FizzBuzz5();
        String result = fizzBUzz5.say(56);
        Assertions.assertEquals("Whizz",result);
    }

    @Test
    public void should_return_Buzz_when_say_given_number_is_mod_by_3_5_and_only_contains_5(){
        FizzBuzz5 fizzBUzz5 = new FizzBuzz5();
        String result = fizzBUzz5.say(15);
        Assertions.assertEquals("Buzz",result);
    }

    @Test
    public void should_return_Whizz_when_say_given_number_is_mod_by_3_7_and_only_contains_5(){
        FizzBuzz5 fizzBUzz5 = new FizzBuzz5();
        String result = fizzBUzz5.say(252);
        Assertions.assertEquals("Whizz",result);
    }

    @Test
    public void should_return_BuzzWhizz_when_say_given_number_is_mod_by_5_7_and_only_contains_5(){
        FizzBuzz5 fizzBUzz5 = new FizzBuzz5();
        String result = fizzBUzz5.say(245);
        Assertions.assertEquals("BuzzWhizz",result);
    }

    @Test
    public void should_return_BuzzWhizz_when_say_given_number_is_mod_by_3_5_7_and_only_contains_5(){
        FizzBuzz5 fizzBUzz5 = new FizzBuzz5();
        String result = fizzBUzz5.say(105);
        Assertions.assertEquals("BuzzWhizz",result);
    }

    @Test
    public void should_return_52_when_say_given_number_is_not_mod_by_3_5_7_and_only_contains_5(){
        FizzBuzz5 fizzBUzz5 = new FizzBuzz5();
        String result = fizzBUzz5.say(52);
        Assertions.assertEquals("52",result);
    }

    @Test
    public void should_return_Fizz_when_say_given_number_is_mod_by_3_and_only_contains_7(){
        FizzBuzz6 fizzBUzz6 = new FizzBuzz6();
        String result = fizzBUzz6.say(27);
        Assertions.assertEquals("Fizz",result);
    }

    @Test
    public void should_return_170_when_say_given_number_is_mod_by_5_and_only_contains_7(){
        FizzBuzz6 fizzBUzz6 = new FizzBuzz6();
        String result = fizzBUzz6.say(170);
        Assertions.assertEquals("170",result);
    }

    @Test
    public void should_return_Whizz_when_say_given_number_is_mod_by_7_and_only_contains_7(){
        FizzBuzz6 fizzBUzz6 = new FizzBuzz6();
        String result = fizzBUzz6.say(7);
        Assertions.assertEquals("Whizz",result);
    }

    @Test
    public void should_return_240_when_say_given_number_is_mod_by_3_5_and_only_contains_7(){
        FizzBuzz6 fizzBUzz6 = new FizzBuzz6();
        String result = fizzBUzz6.say(270);
        Assertions.assertEquals("Fizz",result);
    }

    @Test
    public void should_return_FizzWhizz_when_say_given_number_is_mod_by_3_7_and_only_contains_7(){
        FizzBuzz6 fizzBUzz6 = new FizzBuzz6();
        String result = fizzBUzz6.say(147);
        Assertions.assertEquals("FizzWhizz",result);
    }

    @Test
    public void should_return_Whizz_when_say_given_number_is_mod_by_5_7_and_only_contains_7(){
        FizzBuzz6 fizzBUzz6 = new FizzBuzz6();
        String result = fizzBUzz6.say(70);
        Assertions.assertEquals("Whizz",result);
    }

    @Test
    public void should_return_FizzWhizz_when_say_given_number_is_mod_by_3_5_7_and_only_contains_7(){
        FizzBuzz6 fizzBUzz6 = new FizzBuzz6();
        String result = fizzBUzz6.say(1470);
        Assertions.assertEquals("FizzWhizz",result);
    }

    @Test
    public void should_return_17_when_say_given_number_is_not_mod_by_3_5_7_and_only_contains_7(){
        FizzBuzz6 fizzBUzz6 = new FizzBuzz6();
        String result = fizzBUzz6.say(17);
        Assertions.assertEquals("17",result);
    }

    @Test
    public void should_return_153_when_say_given_number_is_mod_by_3_and_contains_3_5(){
        FizzBuzz7 fizzBUzz7 = new FizzBuzz7();
        String result = fizzBUzz7.say(153);
        Assertions.assertEquals("153",result);
    }

    @Test
    public void should_return_Buzz_when_say_given_number_is_mod_by_5_and_contains_3_5(){
        FizzBuzz7 fizzBUzz7 = new FizzBuzz7();
        String result = fizzBUzz7.say(235);
        Assertions.assertEquals("Buzz",result);
    }

    @Test
    public void should_return_Whizz_when_say_given_number_is_mod_by_7_and_contains_3_5(){
        FizzBuzz7 fizzBUzz7 = new FizzBuzz7();
        String result = fizzBUzz7.say(532);
        Assertions.assertEquals("Whizz",result);
    }

    @Test
    public void should_return_Buzz_when_say_given_number_is_mod_by_3_5_and_contains_3_5(){
        FizzBuzz7 fizzBUzz7 = new FizzBuzz7();
        String result = fizzBUzz7.say(135);
        Assertions.assertEquals("Buzz",result);
    }

    @Test
    public void should_return_Whizz_when_say_given_number_is_mod_by_3_7_and_contains_3_5(){
        FizzBuzz7 fizzBUzz7 = new FizzBuzz7();
        String result = fizzBUzz7.say(1533);
        Assertions.assertEquals("Whizz",result);
    }

    @Test
    public void should_return_BuzzWhizz_when_say_given_number_is_mod_by_5_7_and_contains_3_5(){
        FizzBuzz7 fizzBUzz7 = new FizzBuzz7();
        String result = fizzBUzz7.say(35);
        Assertions.assertEquals("BuzzWhizz",result);
    }

    @Test
    public void should_return_BuzzWhizz_when_say_given_number_is_mod_by_3_5_7_and_contains_3_5(){
        FizzBuzz7 fizzBUzz7 = new FizzBuzz7();
        String result = fizzBUzz7.say(315);
        Assertions.assertEquals("BuzzWhizz",result);
    }

    @Test
    public void should_return_53_when_say_given_number_is_not_mod_by_3_5_7_and_contains_3_5(){
        FizzBuzz7 fizzBUzz7 = new FizzBuzz7();
        String result = fizzBUzz7.say(53);
        Assertions.assertEquals("53",result);
    }

    @Test
    public void should_return_Fizz_when_say_given_number_is_mod_by_3_and_contains_3_7(){
        FizzBuzz8 fizzBUzz8 = new FizzBuzz8();
        String result = fizzBUzz8.say(237);
        Assertions.assertEquals("Fizz",result);
    }

    @Test
    public void should_return_Fizz_when_say_given_number_is_mod_by_5_and_contains_3_7(){
        FizzBuzz8 fizzBUzz8 = new FizzBuzz8();
        String result = fizzBUzz8.say(370);
        Assertions.assertEquals("Fizz",result);
    }

    @Test
    public void should_return_Fizz_when_say_given_number_is_mod_by_7_and_contains_3_7(){
        FizzBuzz8 fizzBUzz8 = new FizzBuzz8();
        String result = fizzBUzz8.say(371);
        Assertions.assertEquals("Fizz",result);
    }

    @Test
    public void should_return_Fizz_when_say_given_number_is_mod_by_3_5_and_contains_3_7(){
        FizzBuzz8 fizzBUzz8 = new FizzBuzz8();
        String result = fizzBUzz8.say(2370);
        Assertions.assertEquals("Fizz",result);
    }

    @Test
    public void should_return_Fizz_when_say_given_number_is_mod_by_3_7_and_contains_3_7(){
        FizzBuzz8 fizzBUzz8 = new FizzBuzz8();
        String result = fizzBUzz8.say(378);
        Assertions.assertEquals("Fizz",result);
    }

    @Test
    public void should_return_Fizz_when_say_given_number_is_mod_by_5_7_and_contains_3_7(){
        FizzBuzz8 fizzBUzz8 = new FizzBuzz8();
        String result = fizzBUzz8.say(3710);
        Assertions.assertEquals("Fizz",result);
    }

    @Test
    public void should_return_Fizz_when_say_given_number_is_mod_by_3_5_7_and_contains_3_7(){
        FizzBuzz8 fizzBUzz8 = new FizzBuzz8();
        String result = fizzBUzz8.say(2370);
        Assertions.assertEquals("Fizz",result);
    }

    @Test
    public void should_return_Fizz_when_say_given_number_is_not_mod_by_3_5_7_and_contains_3_7(){
        FizzBuzz8 fizzBUzz8 = new FizzBuzz8();
        String result = fizzBUzz8.say(37);
        Assertions.assertEquals("Fizz",result);
    }

    @Test
    public void should_return_Fizz_when_say_given_number_is_not_mod_by_3_5_7_and_contains_3_7$2(){
        FizzBuzz8 fizzBUzz8 = new FizzBuzz8();
        String result = fizzBUzz8.say(73);
        Assertions.assertEquals("Fizz",result);
    }

    @Test
    public void should_return_Fizz_when_say_given_number_is_mod_by_3_and_contains_5_7(){
        FizzBuzz9 fizzBUzz9 = new FizzBuzz9();
        String result = fizzBUzz9.say(57);
        Assertions.assertEquals("Fizz",result);
    }

    @Test
    public void should_return_257_when_say_given_number_is_mod_by_5_and_contains_5_7(){
        FizzBuzz9 fizzBUzz9 = new FizzBuzz9();
        String result = fizzBUzz9.say(257);
        Assertions.assertEquals("257",result);
    }

    @Test
    public void should_return_Whizz_when_say_given_number_is_mod_by_7_and_contains_5_7(){
        FizzBuzz9 fizzBUzz9 = new FizzBuzz9();
        String result = fizzBUzz9.say(574);
        Assertions.assertEquals("Whizz",result);
    }

    @Test
    public void should_return_Fizz_when_say_given_number_is_mod_by_3_5_and_contains_5_7(){
        FizzBuzz9 fizzBUzz9 = new FizzBuzz9();
        String result = fizzBUzz9.say(75);
        Assertions.assertEquals("Fizz",result);
    }

    @Test
    public void should_return_FizzWhizz_when_say_given_number_is_mod_by_3_7_and_contains_5_7(){
        FizzBuzz9 fizzBUzz9 = new FizzBuzz9();
        String result = fizzBUzz9.say(567);
        Assertions.assertEquals("FizzWhizz",result);
    }

    @Test
    public void should_return_Whizz_when_say_given_number_is_mod_by_5_7_and_contains_5_7(){
        FizzBuzz9 fizzBUzz9 = new FizzBuzz9();
        String result = fizzBUzz9.say(175);
        Assertions.assertEquals("Whizz",result);
    }

    @Test
    public void should_return_FizzWhizz_when_say_given_number_is_mod_by_3_5_7_and_contains_5_7(){
        FizzBuzz9 fizzBUzz9 = new FizzBuzz9();
        String result = fizzBUzz9.say(1575);
        Assertions.assertEquals("FizzWhizz",result);
    }

    @Test
    public void should_return_157_when_say_given_number_is_not_mod_by_3_5_7_and_contains_5_7(){
        FizzBuzz9 fizzBUzz9 = new FizzBuzz9();
        String result = fizzBUzz9.say(157);
        Assertions.assertEquals("157",result);
    }

    @Test
    public void should_return_Whizz_when_say_given_number_is_mod_by_3_and_contains_3_5_7(){
        FizzBuzz10 fizzBUzz10 = new FizzBuzz10();
        String result = fizzBUzz10.say(3157);
        Assertions.assertEquals("Whizz",result);
    }

    @Test
    public void should_return_Fizz_when_say_given_number_is_mod_by_5_and_contains_3_5_7(){
        FizzBuzz10 fizzBUzz10 = new FizzBuzz10();
        String result = fizzBUzz10.say(375);
        Assertions.assertEquals("Fizz",result);
    }

    @Test
    public void should_return_FizzWhizz_when_say_given_number_is_mod_by_7_and_contains_3_5_7(){
        FizzBuzz10 fizzBUzz10 = new FizzBuzz10();
        String result = fizzBUzz10.say(357);
        Assertions.assertEquals("FizzWhizz",result);
    }

    @Test
    public void should_return_Whizz_when_say_given_number_is_mod_by_3_5_and_contains_3_5_7(){
        FizzBuzz10 fizzBUzz10 = new FizzBuzz10();
        String result = fizzBUzz10.say(3745);
        Assertions.assertEquals("Whizz",result);
    }

    @Test
    public void should_return_FizzWhizz_when_say_given_number_is_mod_by_5_7_and_contains_3_5_7() {
        FizzBuzz10 fizzBUzz10 = new FizzBuzz10();
        String result = fizzBUzz10.say(735);
        Assertions.assertEquals("FizzWhizz", result);
    }

    @Test
    public void should_return_1357_when_say_given_number_is_not_mod_by_3_5_7_and_contains_3_5_7() {
        FizzBuzz10 fizzBUzz10 = new FizzBuzz10();
        String result = fizzBUzz10.say(1357);
        Assertions.assertEquals("1357", result);
    }
}
