﻿# TDD @FizzBuzz

## 开发环境
 - JDK11+
 
## 业务目标

### FizzBuzz
设计一个程序，该程序能打印1到100之间的数字，包含1，100。
- Given 需要打印的数字是6（3的倍数），When 打印，Then 打印`"Fizz"`
- Given 需要打印的数字是10（5的倍数），When 打印，Then 打印`"Buzz"`
- Given 需要打印的数字是15（同时是3和5的倍数），When 打印，Then 打印`"FizzBuzz"`
- Given 需要打印的数字是2（不是3或者5的倍数），When 打印，Then 打印`"2"`
- Given 需要打印的数字是100（超出限制），When 打印，Then 抛出异常

## Tasking

| Task | Input | Output |
|:---|:---|:---|
| 1 | 15, 30, 45, 60, 75, 90 |  `"FizzBuzz"` |
| 2 | 3, 6, 12, 18, 21, 99 |  `"Fizz"` |
| 3 | 5, 10, 20, 85, 95, 100 |  `"Buzz"` |
| 4 | 1, 2, 4, 94, 97, 98 |  `"1"`, `"2"`, `"4"`, `"94"`, `"97"`, `"98"` |
| 5 | 0, 101 |  `IllegalArgumentException` |


## 编码路线
`master`分支一共有5个tag（`Task1`，`Task2`，`Task3`，`Task4`，`Task5`），它们分别对应5个Task。Clone下代码库之后，`checkout`到最开始的提交，一步一步往后面`checkout`即可查看所有步骤，`Task4`与`Task5`之间有1次Refactor的`commit`。


## 参考资料
- [JUnit 5用户指南](https://gitee.com/liushide/junit5_cn_doc/blob/master/junit5UserGuide_zh_cn.md#https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Fjunit-team%2Fjunit5-samples%2Ftree%2Fr5.0.2%2Fjunit5-gradle-consumer)
- [Gradle 用户指南](https://docs.gradle.org/current/userguide/userguide.html)


/*普通数字*/
Given 数字1满足不包含3，5，7且不能被3，5，7整除 When 报数 Then 学生报1
Given 数字4满足不包含3，5，7且不能被3，5，7整除 When 报数 Then 学生报4

/*3, 5, 7的倍数*/
Given 数字6满足只被3整除，且不包含3，5，7 When 报数 Then 学生报Fizz
Given 数字10满足只被5整除，且不包含3，5，7 When 报数 Then 学生报Buzz
Given 数字14满足只被7整除，且不包含3，5，7 When 报数 Then 学生报Whizz

/*15, 21, 35, 105的倍数*/
Given 数字60满足同时被3和5整除，且不包含3，5，7 When 报数 Then 学生报FizzBuzz
Given 数字21满足同时被3和7整除，且不包含3，5，7 When 报数 Then 学生报FizzWhizz
Given 数字140满足同时被5和7整除，且不包含3，5，7 When 报数 Then 学生报BuzzWhizz
Given 数字210满足同时被3和5和7整除，且不包含3，5，7 When 报数 Then 学生报FizzBuzzWhizz

/*包含3*/
Given 数字3满足只被3整除，且只包含3 When 报数 Then 学生报Fizz
Given 数字130满足只被5整除，且只包含3 When 报数 Then 学生报Fizz
Given 数字133满足只被7整除，且只包含3 When 报数 Then 学生报Fizz
//Given 数字30满足同时被3和5整除，且只包含3 When 报数 Then 学生报Fizz
//Given 数字63满足同时被3和7整除，且只包含3 When 报数 Then 学生报Fizz
//Given 数字630满足同时能被3和5和7整除，且只包含3 When 报数 Then 学生报Fizz
//Given 数字1330不能被3或5或7整除，且只包含3 When 报数 Then 学生报Fizz
Given 数字13不能被3或5或7整除，且只包含3 When 报数 Then 学生报Fizz

/*包含5*/
Given 数字51满足只被3整除，且只包含5 When 报数 Then 学生报51
//Given 数字5满足只能被5整除，且只包含5 When 报数 Then 学生报Buzz
Given 数字56满足只被7整除，且只包含5 When 报数 Then 学生报Whizz
Given 数字15满足同时被3和5整除，且只包含5 When 报数 Then 学生报Buzz
//Given 数字252满足同时被3和7整除，且只包含5 When 报数 Then 学生报Whizz
Given 数字245满足同时被5和7整除，且只包含5 When 报数 Then 学生报BuzzWhizz
//Given 数字105满足同时被3和5和7整除，且只包含5 When 报数 Then 学生报BuzzWhizz
//Given 数字52不能被3或5或7整除，且只包含5 When 报数 Then 学生报52


/*包含7*/
//Given 数字27满足只被3整除，且只包含7 When 报数 Then 学生报Fizz
//Given 数字170满足只被5整除，且只包含7 When 报数 Then 学生报170
Given 数字7满足只被7整除，且只包含7 When 报数 Then 学生报Whizz
Given 数字270满足同时被3和5整除，且只包含7 When 报数 Then 学生报Fizz
Given 数字147满足同时被3和7整除，且只包含7 When 报数 Then 学生报FizzWhizz
Given 数字70满足同时被5和7整除，且只包含7 When 报数 Then 学生报Whizz
Given 数字1470满足同时被3和5和7整除，且包含7 When 报数 Then 学生报FizzWhizz
Given 数字17不能被3或5或7整除，且只包含7 When 报数 Then 学生报17

/*包含3 5*/
Given 数字153只能被3整除，且包含3，5 When 报数 Then 学生报153
Given 数字235只能被5整除，且包含3，5 When 报数 Then 学生报Buzz
Given 数字532只能被7整除，且包含3，5 When 报数 Then 学生报Whizz
Given 数字135能同时被3和5整除，且包含3，5 When 报数 Then 学生报Buzz
Given 数字1533能同时被3和7整除，且包含3，5 When 报数 Then 学生报Whizz
Given 数字35能同时被5和7整除，且包含3，5 When 报数 Then 学生报BuzzWhizz
Given 数字315能同时被3和5和7整除，且包含3，5 When 报数 Then 学生报BuzzWhizz
Given 数字53不能被3或5或7整除，且包含3，5 When 报数 Then 学生报53

/*包含3 7*/
Given 数字237只能被3整除，且包含3，7 When 报数 Then 学生报Fizz
Given 数字370只能被5整除，且包含3，7 When 报数 Then 学生报Fizz
Given 数字371只能被7整除，且包含3，7 When 报数 Then 学生报Fizz
Given 数字2370能同时被3和5整除，且包含3，7 When 报数 Then 学生报Fizz
Given 数字378能同时被3和7整除，且包含3，7 When 报数 Then 学生报Fizz
Given 数字3710能同时被5和7整除，且包含3，7 When 报数 Then 学生报Fizz
Given 数字2370能同时被3和5和7整除，且包含3，7 When 报数 Then 学生报Fizz
Given 数字37不能被3或5或7整除，且包含3，7 When 报数 Then 学生报Fizz
Given 数字73不能被3或5或7整除，且包含3，7 When 报数 Then 学生报Fizz

/*包含5 7*/
Given 数字57只能被3整除，且包含5，7 When 报数 Then 学生报Fizz
Given 数字257只能被5整除，且包含5，7 When 报数 Then 学生报257
Given 数字574只能被7整除，且包含5，7 When 报数 Then 学生报Whizz
Given 数字75能同时被3和5整除，且包含5，7 When 报数 Then 学生报Fizz
Given 数字567能同时被3和7整除，且包含5，7 When 报数 Then 学生报FizzWhizz
Given 数字175能同时被5和7整除，且包含5，7 When 报数 Then 学生报Whizz
Given 数字1575能同时被3和5和7整除，且包含5，7 When 报数 Then 学生报FizzWhizz
Given 数字157不能被3或5或7整除，且包含5，7 When 报数 Then 学生报157

/*包含3 5 7*/
Given 数字3157只能被3整除，且包含3，5，7 When 报数 Then 学生报Whizz
Given 数字375只能被5整除，且包含3，5，7 When 报数 Then 学生报Fizz
Given 数字357只能被7整除，且包含3，5，7 When 报数 Then 学生报FizzWhizz
Given 数字3745能同时被3和5整除，且包含3，5，7 When 报数 Then 学生报Whizz
Given 数字735能同时被5和7整除，且包含3，5，7 When 报数 Then 学生报FizzWhizz
Given 数字1357不能被3或5或7整除，且包含3,5，7 When 报数 Then 学生报1357







